<?php

namespace App\Model\Entities\Promo;

use App\Model\Entities\Traits\HasChildren;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Model\Entities\Promo\Reward
 *
 * @property int                                     $id
 * @property int                                     $campaign_id
 * @property string                                  $description
 * @property \Illuminate\Support\Carbon              $created_at
 * @property \Illuminate\Support\Carbon|null         $updated_at
 * @property-read \App\Model\Entities\Promo\Campaign $campaign
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward whereCampaignId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Reward whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reward extends Model
{

	use HasChildren;

	protected $table = 'promo_rewards';

	protected $fillable = [
		'description',
	];

	protected $dates = [
		'created_at',
		'updated_at',
	];

	public function campaign()
	{
		return $this->belongsTo(Campaign::class);
	}
}
