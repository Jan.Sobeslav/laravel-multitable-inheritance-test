<?php

namespace App\Model\Entities\Promo;

use App\Model\Entities\Calendar\Campaign as CalendarCampaign;
use App\Model\Entities\Promo\Campaign as PromoCampaign;
use App\Model\Entities\Traits\HasChildren;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Entities\Promo\Campaign
 *
 * @property int                                                                              $id
 * @property string                                                                           $description
 * @property \Illuminate\Support\Carbon                                                       $valid_from
 * @property \Illuminate\Support\Carbon                                                       $valid_to
 * @property \Illuminate\Support\Carbon                                                       $created_at
 * @property \Illuminate\Support\Carbon|null                                                  $updated_at
 * @property-read mixed                                                                       $state
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Entities\Promo\Reward[] $rewards
 * @property-read int|null                                                                    $rewards_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereValidFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Promo\Campaign whereValidTo($value)
 * @mixin \Eloquent
 */
class Campaign extends Model
{

	use HasChildren;

	protected $table = 'promo_campaigns';

	protected $fillable = [
		'description',

		'valid_from',
		'valid_to',
	];

	protected $dates = [
		'valid_from',
		'valid_to',

		'created_at',
		'updated_at',
	];

	public function rewards(): HasMany
	{
		return $this->hasMany(Reward::class);
	}

	public function getStateAttribute()
	{
		return $this->getState(Carbon::now());
	}

	/**
	 * Get campaign status on current timestamp.
	 *
	 * @param Carbon $timestamp
	 *
	 * @return string
	 */
	public function getState(Carbon $timestamp): string
	{
		if ($this->valid_from > $timestamp) {
			return 'scheduled';
		}

		if ($this->valid_from < $timestamp && $this->valid_to > $timestamp) {
			return 'running';
		}

		if ($this->valid_to < $timestamp) {
			return 'ended';
		}

		return 'unknown';
	}
}
