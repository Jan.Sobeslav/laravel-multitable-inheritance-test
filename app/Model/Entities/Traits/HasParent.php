<?php

namespace App\Model\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

/**
 * Trait HasParent
 *
 * @package App\Model\Entities\Traits
 *
 * @property $parentModelClass
 * @property $inherits
 */
trait HasParent
{

	/**
	 * Relationship with parent model.
	 *
	 * @return BelongsTo
	 */
	public function parentModel(): BelongsTo
	{
		return $this->belongsTo(static::$parentModelClass, 'parent_id');
	}

	/**
	 * Get attribute either from child or parent.
	 *
	 * @param $key
	 *
	 * @return mixed
	 */
	public function __get($key)
	{
		// If the field shouldn't be inherited, return child property..
		if (! in_array($key, static::$inherits)) {
			return parent::__get($key);
		}

		// Otherwise return parent property.
		return $this->parentModel->__get($key);
	}

	/**
	 * Create new model pair.
	 *
	 * @param array $attributes
	 *
	 * @return static
	 */
	public static function create($attributes = [])
	{
		// Create parent.
		$parentAttributes = static::getParentAttributes($attributes);
		$parentModel      = static::$parentModelClass::create($parentAttributes);

		// Create child.
		$childAttributes              = static::getChildAttributes($attributes);
		$childAttributes['parent_id'] = $parentModel->id;

		// Note: simple `parent::create()` would not work because of method `Model::__callStatic()`
		$child = parent::make();
		$child->fill($childAttributes);
		$child->save();

		return $child;
	}

	/**
	 * Update the model pair in the database.
	 *
	 * @param array $attributes
	 * @param array $options
	 *
	 * @return bool
	 */
	public function update(array $attributes = [], array $options = [])
	{
		// Update parent.
		$parentAttributes = static::getParentAttributes($attributes);
		$parentModel      = $this->parentModel->update($parentAttributes);

		// Update child.
		$childAttributes = static::getChildAttributes($attributes);
		return parent::update($childAttributes, $options);
	}

	/**
	 * Delete the model from the database.
	 *
	 * @return bool|null
	 *
	 * @throws \Exception
	 */
	public function delete()
	{
		$this->parentModel->delete();
		parent::delete();
	}

	/**
	 * Filter out attributes that should be saved to parent table.
	 *
	 * @param array $attributes
	 *
	 * @return array
	 */
	protected static function getParentAttributes(array $attributes)
	{
		return Arr::only($attributes, static::$inherits);
	}

	/**
	 * Filter out attributes that should be saved to child table.
	 *
	 * @param array $attributes
	 *
	 * @return array
	 */
	protected static function getChildAttributes(array $attributes)
	{
		return Arr::except($attributes, static::$inherits);
	}
}