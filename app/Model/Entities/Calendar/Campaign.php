<?php

namespace App\Model\Entities\Calendar;

use App\Model\Entities\Promo\Campaign as PromoCampaign;
use App\Model\Entities\Traits\HasParent;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Model\Entities\Calendar\Campaign
 *
 * @property int                                                                                 $id
 * @property int                                                                                 $parent_id
 * @property \Illuminate\Support\Carbon                                                          $calendar_from
 * @property-read mixed                                                                          $state
 * @property-read \App\Model\Entities\Promo\Campaign                                             $parentModel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Entities\Calendar\Reward[] $rewards
 * @property-read int|null                                                                       $rewards_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign whereCalendarFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Campaign whereParentId($value)
 * @mixin \Eloquent
 */
class Campaign extends PromoCampaign
{

	use HasParent;

	protected $table = 'calendar_campaigns';

	protected $fillable = [
		'calendar_from',
		'parent_id',
	];

	protected $dates = [
		'calendar_from',
	];

	public $timestamps = false;

	protected static $parentModelClass = PromoCampaign::class;

	protected static $inherits = [
		'description',
		'valid_from',
		'valid_to',
		'created_at',
		'updated_at',
	];

	public function rewards(): HasMany
	{
		return $this->hasMany(Reward::class);
	}
}
