<?php

namespace App\Model\Entities\Calendar;

use App\Model\Entities\Promo\Reward as PromoReward;
use App\Model\Entities\Traits\HasParent;

/**
 * App\Model\Entities\Calendar\Reward
 *
 * @property int                                        $id
 * @property int                                        $parent_id
 * @property-read \App\Model\Entities\Calendar\Campaign $campaign
 * @property-read \App\Model\Entities\Promo\Reward      $parentModel
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Reward newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Reward newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Reward query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Entities\Calendar\Reward whereParentId($value)
 * @mixin \Eloquent
 */
class Reward extends PromoReward
{

	use HasParent;

	protected $table = 'calendar_rewards';

	protected $fillable = [
		'parent_id',
	];

	public $timestamps = false;

	protected static $parentModelClass = PromoReward::class;

	protected static $inherits = [
		'campaign_id',
		'description',
		'created_at',
		'updated_at',
	];

	public function campaign()
	{
		return $this->belongsTo(Campaign::class);
	}
}
