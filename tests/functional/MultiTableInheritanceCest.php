<?php

use App\Model\Entities\Calendar\Campaign as CalendarCampaign;
use App\Model\Entities\Promo\Campaign as PromoCampaign;
use Carbon\Carbon;

class MultiTableInheritanceCest
{

	protected const PROMO_CAMPAIGN_ID    = 2;
	protected const CALENDAR_CAMPAIGN_ID = 3;
	protected const PROMO_REWARD_ID      = 4;
	protected const CALENDAR_REWARD_ID   = 5;

	protected function setupReadMock(FunctionalTester $I)
	{
		// Setup campaign.
		$I->haveInDatabase('promo_campaigns', [
			'id'          => static::PROMO_CAMPAIGN_ID,
			'description' => 'read test campaign',
			'valid_from'  => '2020-07-18 13:10:36',
			'valid_to'    => '2021-07-18 13:10:36',
		]);
		$I->haveInDatabase('calendar_campaigns', [
			'id'            => static::CALENDAR_CAMPAIGN_ID,
			'parent_id'     => static::PROMO_CAMPAIGN_ID,
			'calendar_from' => '2020-07-18 13:10:36',
		]);

		// Setup reward.
		$I->haveInDatabase('promo_rewards', [
			'id'          => static::PROMO_REWARD_ID,
			'campaign_id' => static::PROMO_CAMPAIGN_ID,
			'description' => 'read test campaign',
		]);
		$I->haveInDatabase('calendar_rewards', [
			'id'        => static::CALENDAR_REWARD_ID,
			'parent_id' => static::PROMO_REWARD_ID,
		]);
	}

	/**
	 * @group create
	 */
	public function createChild(FunctionalTester $I)
	{
		$calendarCampaign = CalendarCampaign::create([
			'description'   => 'create test campaign',
			'valid_from'    => '2020-07-18 13:10:36',
			'valid_to'      => '2021-07-18 13:10:36',
			'calendar_from' => '2020-07-18 13:10:36',
		]);

		// I see both child and parent records.
		$I->seeInDatabase('calendar_campaigns', [
			'parent_id' => $calendarCampaign->parent_id,
		]);
		$I->seeInDatabase('promo_campaigns', [
			'id'          => $calendarCampaign->parent_id,
			'description' => 'create test campaign',
		]);
	}

	/**
	 * @group create
	 */
	public function createParentAlone(FunctionalTester $I)
	{
		$promoCampaign = PromoCampaign::create([
			'description' => 'create test campaign',
			'valid_from'  => '2020-07-18 13:10:36',
			'valid_to'    => '2021-07-18 13:10:36',
		]);

		// I can see parent record.
		$I->seeInDatabase('promo_campaigns', [
			'description' => 'create test campaign',
		]);
	}

	/**
	 * @group read
	 */
	public function readChildPropTroughChild(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$calendarCampaign = CalendarCampaign::find(static::CALENDAR_CAMPAIGN_ID);

		$I->assertEquals(
			'2020-07-18',
			$calendarCampaign->calendar_from->format('Y-m-d')
		);
	}

	/**
	 * @group read
	 */
	public function readParentPropTroughChild(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$calendarCampaign = CalendarCampaign::find(static::CALENDAR_CAMPAIGN_ID);

		$I->assertEquals(
			'read test campaign',
			$calendarCampaign->description
		);
	}

	/**
	 * @group read
	 */
	public function callParentMethodTroughChild(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$calendarCampaign = CalendarCampaign::find(static::CALENDAR_CAMPAIGN_ID);

		$I->assertEquals('running',
			$calendarCampaign->getState(
				Carbon::parse('2020-12-18 13:10:36')
			)
		);
	}

	/**
	 * @group update
	 */
	public function updateParentPropTroughChild(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$calendarCampaign = CalendarCampaign::find(static::CALENDAR_CAMPAIGN_ID);

		$calendarCampaign->update([
			'description' => 'update test campaign',
		]);

		$I->seeInDatabase('promo_campaigns', [
			'id'          => $calendarCampaign->parent_id,
			'description' => 'update test campaign',
		]);
	}

	/**
	 * @group update
	 */
	public function updateParentPropTroughParent(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$promoCampaign = PromoCampaign::find(static::PROMO_CAMPAIGN_ID);

		$promoCampaign->update([
			'description' => 'update test campaign',
		]);

		$I->seeInDatabase('promo_campaigns', [
			'id'          => $promoCampaign->id,
			'description' => 'update test campaign',
		]);
	}

	/**
	 * @group delete
	 */
	public function deletePairTroughChild(FunctionalTester $I)
	{
		$this->setupReadMock($I);
		/** @var CalendarCampaign $calendarCampaign */
		$calendarCampaign = CalendarCampaign::find(static::CALENDAR_CAMPAIGN_ID);

		$calendarCampaign->delete();

		$I->dontSeeInDatabase('calendar_campaigns',
			[
				'id' => self::CALENDAR_CAMPAIGN_ID,
			]
		);
		$I->dontSeeInDatabase('promo_campaigns',
			[
				'id' => self::PROMO_CAMPAIGN_ID,
			]
		);
	}

}
