<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarCampaign extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_campaigns', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('parent_id');

			$table->timestamp('calendar_from');

			$table->foreign('parent_id', 'fx_calendar_campaigns_parent_id_promo_campaigns_id')
				  ->references('id')
				  ->on('promo_campaigns');
		});

		Schema::create('calendar_rewards', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('parent_id');

			$table->foreign('parent_id', 'fx_calendar_rewards_parent_id_promo_rewards_id')
				  ->references('id')
				  ->on('promo_rewards');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calendar_rewards');
		Schema::dropIfExists('calendar_campaigns');
	}
}
