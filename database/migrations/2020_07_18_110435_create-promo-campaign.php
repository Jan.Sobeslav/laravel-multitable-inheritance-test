<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoCampaign extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create base campaign.
		Schema::create('promo_campaigns', function (Blueprint $table) {
			$table->increments('id');

			$table->string('description', 255);

			$table->timestamp('valid_from');
			$table->timestamp('valid_to');

			$table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->nullable();
		});

		// Create base reward.
		Schema::create('promo_rewards', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('campaign_id');

			$table->string('description', 255);

			$table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->nullable();

			$table->foreign('campaign_id', 'fx_promo_rewards_campaign_id_promo_campaigns_id')
				  ->references('id')
				  ->on('promo_campaigns');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('promo_rewards');
		Schema::dropIfExists('promo_campaigns');
	}
}
